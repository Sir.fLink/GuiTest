package de.deepgame.arimaa.gui.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import de.deepgame.arimaa.gui.shared.GuiPiece;

/**
 * The client-side stub for the RPC service.
 */
@RemoteServiceRelativePath("start")
public interface StartGameService extends RemoteService {
	String greetServer(String name) throws IllegalArgumentException;

	String sendStartConfig(GuiPiece[] guiStartConfiguration);
	
	GuiPiece[] getOpponentStartConfig();
}
