package de.deepgame.arimaa.gui.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

import de.deepgame.arimaa.gui.shared.GuiPiece;

/**
 * The async counterpart of <code>StartGameService</code>.
 */
public interface StartGameServiceAsync {
	void greetServer(String input, AsyncCallback<String> callback)
			throws IllegalArgumentException;
	
	void sendStartConfig(GuiPiece[] guiStartConfiguration, AsyncCallback<String> callback);

	void getOpponentStartConfig(AsyncCallback<GuiPiece[]> callback);
}
