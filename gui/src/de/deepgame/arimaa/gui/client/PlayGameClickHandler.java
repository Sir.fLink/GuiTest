package de.deepgame.arimaa.gui.client;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Grid;

public class PlayGameClickHandler implements ClickHandler{

	private Grid boardGripRepresentation;
	private Board board;
	
	private int clickedPieceX;
	private int clickedPieceY;
	
	@Override
	public void onClick(ClickEvent event) {
		int xOfClick = boardGripRepresentation.getCellForEvent(event)
				.getCellIndex();
		int yOfClick = boardGripRepresentation.getCellForEvent(event)
				.getRowIndex();
		
		Window.alert("in");
		
		if(board.getChoosenPiece() == null){
			
			
		}else{
			board.setChoosenPiece(null);
			
			clickedPieceX = xOfClick;
			clickedPieceY = yOfClick;
		}
	}
	
	public void setBoardGridRepresentation(Grid boardGridRepresentation){
		this.boardGripRepresentation = boardGridRepresentation;
	}
	
	public void setBoard(Board board){
		this.board = board;
	}

}
