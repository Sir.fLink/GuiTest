package de.deepgame.arimaa.gui.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.RootPanel;

import de.deepgame.arimaa.gui.server.GuiPlayer;
import de.deepgame.arimaa.gui.shared.GuiPiece;




/**
 * The representation of the Board in the gui Holds the state and communicates
 * Changes to the server
 * 
 * Is a Singelton
 * 
 * @author haukeedeler
 *
 */

public class GuiBoard {
	private static Grid boardGridRepresentation;
	private static GuiBoard instance;
	private ClickHandler startConfigurationClickHandler;
	private ClickHandler startConfigurationBoardClickHandler;
	// Stores the last clicked PiecesBoxPanel
	private SinglePieceBoxPanel lastUsedPiecesBoxPanel;
	private int numberOfPiecesInTheStartConfigArray;
	private StartGameServiceAsync startGameServiceAsync;

	private boolean settingStartConfig = true;
	private int xSelectedPiece;
	private int ySelectedPiece;
	private boolean pieceSelected = false;
	private String tmpImg;
	
	// The array used to send the startconfiguration to the server
	private GuiPiece[] startConfiguration = new GuiPiece[16];

	
	
	/**
	 * The handler registration on the Board (allows putting the pieces from the
	 * pieces Box on the field or choose a piece). This registration is _only_
	 * used to destroy the Handlers, when other handlers are attached
	 */
	private HandlerRegistration handlerRegistrationConfigHandlerOnTheBoard;
	private HandlerRegistration handlerRegistrationPutPiecesOnTheBoard;

	/**
	 * The Type (Rabbit, Horse, etc.) of the piece choosen by the User
	 */
	GuiPiece choosenPiece;

	/**
	 * The Position of the Piece chossen by the User, based in the upper left
	 * corner Stays at -1 during the user setup of the Start-Configuration
	 */
	int xOfChoosenPiece = -1;
	int yOfChoosenPiece = -1;

	/**
	 * The coordinates of the Field choosen by the Player
	 */
	int xOfChoosenField = -1;
	int yOfChoosenField = -1;

	GuiBoard() {
		// graphical representation of the Board
		boardGridRepresentation = new Grid(8, 8);

		setBoardtoStandardLook();

		startConfigurationClickHandler = new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				
			}
		};
	}

		
	

	/**
	 * Sets the board to the Standard-Look
	 */
	public void setBoardtoStandardLook() {
		// This hole thing only creates the Board and sets the CSS Styles for
		// the Elements
		boolean evenrow = false;
		boolean evencolumn = false;

		for (int rows = 0; rows < 8; rows++) {
			for (int columns = 0; columns < 8; columns++) {

				if (evenrow && evencolumn) {
					boardGridRepresentation.getCellFormatter().setStyleName(
							rows, columns, "field_black");
				} else if (evenrow && !evencolumn) {
					boardGridRepresentation.getCellFormatter().setStyleName(
							rows, columns, "field_white");
				} else if (!evenrow && evencolumn) {
					boardGridRepresentation.getCellFormatter().setStyleName(
							rows, columns, "field_white");
				} else { // !evenrow && !evencolumn
					boardGridRepresentation.getCellFormatter().setStyleName(
							rows, columns, "field_black");
				}
				if (evencolumn) {
					evencolumn = false;
				} else {
					evencolumn = true;
				}
			}
			if (evenrow) {
				evenrow = false;
			} else {
				evenrow = true;
			}
			;
		}
		// it's a trap!
		boardGridRepresentation.getCellFormatter().setStyleName(2, 2,
				"field_trap");
		boardGridRepresentation.getCellFormatter().setStyleName(2, 5,
				"field_trap");
		boardGridRepresentation.getCellFormatter().setStyleName(5, 2,
				"field_trap");
		boardGridRepresentation.getCellFormatter().setStyleName(5, 5,
				"field_trap");

	}

	/**
	 * Sets the picture of the Piece on the Board is Zero-Based
	 *
	 * 
	 * @param piece
	 * @param xcord
	 * @param ycord
	 */

	public void setPieceOnTheBoardDuringStartConfig(GuiPiece piece, int xcord,
			int ycord) {
		if (ycord < 6) {
			Window.alert("you cant put a piece here!");
		} else {
			boardGridRepresentation.setWidget(ycord, xcord, piece.getImage());
			numberOfPiecesInTheStartConfigArray += 1;
			instance.setToStartConfigurationClickHandler();
		}
	}
	
	public void movePieceFromTo(String piece, int xOld, int yOld,int  xNew, int yNew){
		boardGridRepresentation.setWidget(yOld, xOld, null);
		boardGridRepresentation.setWidget(yNew, xNew, new Image(piece));
	}

	/**
	 * Is called in the Setup-Stage to fade out the fields the Player is not
	 * supposed to put any pieces on
	 */
	public void setFadeForStartConfig() {
		// Fade out the upper 6 rows
		for (int rows = 0; rows < 6; rows++) {
			for (int columns = 0; columns < 8; columns++) {
				boardGridRepresentation.getCellFormatter().setStyleName(rows,
						columns, "field_faded");
			}
		}
	}

	/**
	 * Click handler used on the board to allow the user setting a Piece Needs
	 * the calling PiecesBoxPanel to decrement the counter onClick
	 */
	public void setToPutPieceClickHandler(
			SinglePieceBoxPanel singlePieceBoxPanel) {

		
		// Stores the last used piecesBoxPanel to decrement the counter
		// onClick()
		lastUsedPiecesBoxPanel = singlePieceBoxPanel;

		handlerRegistrationConfigHandlerOnTheBoard.removeHandler();

		startConfigurationBoardClickHandler = new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				int xOfClick = boardGridRepresentation.getCellForEvent(event)
						.getCellIndex();
				int yOfClick = boardGridRepresentation.getCellForEvent(event)
						.getRowIndex();
				
				// the following "if" prevents users from Setting pieces above
				// the lower two rows and also to place a piece on a field that
				// has already been take by another piece.

				// TODO this is not so nice since swaping the pieces might be
				// the better choice...
				
				if (!(yOfClick < 6) 
						&& startConfiguration[calculatePostionInStartConfig(
								xOfClick, yOfClick)] == null && settingStartConfig) {
					// Set the Piece on the field
					instance.setPieceOnTheBoardDuringStartConfig(choosenPiece,
							xOfClick, yOfClick);
					// decrement the Counter for the Number of pieces Left
					lastUsedPiecesBoxPanel.decrementCounter();

					// Store the Piece in the StartConfig Array for the GC
					startConfiguration[calculatePostionInStartConfig(xOfClick,
							yOfClick)] = choosenPiece;

					// Dirty workaround to fix my Click-Handler Clusterfuck. The
					// TODO Clickhandler flow has to be rewritten... >>>
					handlerRegistrationPutPiecesOnTheBoard.removeHandler();
					choosenPiece = null;
					lastUsedPiecesBoxPanel = null;
					// <<<

				}
			}
		};

		handlerRegistrationPutPiecesOnTheBoard = boardGridRepresentation
				.addClickHandler(startConfigurationBoardClickHandler);
	}

	/**
	 * Since the StartConfiguration is stored as an one-dimensional array, the
	 * position in the Array has to be calculated accordingly. Assumes by
	 * convention that the first piece is in the lower left corner...
	 * 
	 * @param xPosition
	 * @param yPostion
	 * @return
	 */

	private int calculatePostionInStartConfig(int xPosition, int yPostion) {
		if (yPostion == 7) {
			return xPosition;
		} else if (yPostion == 6) {
			return (xPosition + 8);
		} else {
			Window.alert("How on earth did you put a piece there?");
			return -1;
		}
	}

	public void setToStartConfigurationClickHandler() {
		handlerRegistrationPutPiecesOnTheBoard.removeHandler();
		boardGridRepresentation.addClickHandler(startConfigurationClickHandler);
	}

	/**
	 * Sends the startconfig off to the Server and also gets the opponents
	 * startconfig which in a separate function is also added to the board
	 * 
	 * @return true if the startConfig was sent (due to the Async nature of the
	 *         call this does not mean that the server actually took it. This
	 *         has to be determinant by the Callback)
	 */
	public boolean sendStartConfigToTheServer() {
		/**
		 * Create a remote service proxy to talk to the server-side service.
		 */
		if (startGameServiceAsync == null) {
			startGameServiceAsync = GWT.create(StartGameService.class);
		}

		if (numberOfPiecesInTheStartConfigArray != 16) {
			// TODO: Allow the player to play with a Handicap (aka fewer pieces)
			Window.alert("You have to put all Pieces on the board!");
			return false;
		} else {
			startGameServiceAsync.sendStartConfig(startConfiguration,
					new AsyncCallback<String>() {

						@Override
						public void onSuccess(String result) {
							// TODO The Result could be logged...

							// if setting the StartConfig on the Server is
							// successful, retrieve the opponents startconfig
							// from the Server

							getOpponentStartConfigFromTheServer();
							settingStartConfig = false;
						}

						@Override
						public void onFailure(Throwable caught) {
							Window.alert("Communication went wrong while sendig the StartConfiguration to the Server");

						}
					});
		}
		return true;
	}

	/**
	 * Calls the startGameService to get the Opponents StartConfig. Then Calls
	 * setOpponentsStartConfigurationToTheBoard with the result.
	 */
	private void getOpponentStartConfigFromTheServer() {
		startGameServiceAsync
				.getOpponentStartConfig(new AsyncCallback<GuiPiece[]>() {

					@Override
					public void onSuccess(GuiPiece[] result) {
						// Set the board to standard-look
						GuiBoard.getInstance().setBoardtoStandardLook();
						// Remove the Pieces Box from Screen
						PiecesBoxPanel.getInstance().removeFromParent();

						// Set the opponents
						// StartConfiguration on the Board
						setOpponentsStartConfigurationToTheBoard(result);

					}

					@Override
					public void onFailure(Throwable caught) {
						Window.alert("Was not able to get the opponents StartConfig from the Server.");

					}
				});
	}

	/**
	 * Sets a Startconfiguration to the Board
	 * 
	 * @param startConfigToBeAdded
	 */

	private void setOpponentsStartConfigurationToTheBoard(
			GuiPiece[] startConfigToBeAdded) {

		for (int i = 0; i < startConfigToBeAdded.length; i++) {
			if (i < 8) {
				boardGridRepresentation.setWidget(0, i,
						startConfigToBeAdded[i].getImage());
			} else {
				boardGridRepresentation.setWidget(1, i - 8,
						startConfigToBeAdded[i].getImage());
			}
		}

	}

	/**
	 * Tells the board to attach itself to the rootPanel
	 */
	public void addBoardToRootPanel() {
		RootPanel.get("board").add(boardGridRepresentation);
	}

	/*
	 * Setters and Getters
	 */

	public static GuiBoard getInstance() {
		if (instance == null) {
			instance = new GuiBoard();
		}

		return instance;
	}

	public GuiPiece getChoosenPiece() {
		return choosenPiece;
	}

	public void setChoosenPiece(GuiPiece choosenPiece) {
		this.choosenPiece = choosenPiece;
	}

	public int getxOfChoosenField() {
		return xOfChoosenField;
	}

	public void setxOfChoosenField(int xOfChoosenField) {
		this.xOfChoosenField = xOfChoosenField;
	}

	public int getyOfChoosenField() {
		return yOfChoosenField;
	}

	public void setyOfChoosenField(int yOfChoosenField) {
		this.yOfChoosenField = yOfChoosenField;
	}

}
