package de.deepgame.arimaa.gui.client;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.RootPanel;

import de.deepgame.arimaa.gui.shared.GuiPiece;

public class Board {
	
	private static Board instanceOfBoard;
	private static Grid boardGridRepresentation;
	
	private HandlerRegistration handlerRegistration;
	private ClickHandler clickHandler;

	private GuiPiece[] startConfiguration = new GuiPiece[16];
	private GuiPiece choosenPiece = null;
	
	private SinglePieceBoxPanel lastClickedBox;
	
	private Board(){
		
		boardGridRepresentation = new Grid(8, 8);
		
		setBoardtoStandardLook();
		setFadeForStartConfig();
		
		clickHandler = new StartConfigClickHandler();	
		((StartConfigClickHandler) clickHandler).setBoardGridRepresentation(boardGridRepresentation);
		((StartConfigClickHandler) clickHandler).setBoard(this);

		
		handlerRegistration = boardGridRepresentation.
				addClickHandler(clickHandler);
	}
	
	public static Board getInstance(){
		if(instanceOfBoard == null){
			instanceOfBoard = new Board();
		}
		
		return instanceOfBoard;
	}
	
	public void setFadeForStartConfig() {
		// Fade out the upper 6 rows
		for (int rows = 0; rows < 6; rows++) {
			for (int columns = 0; columns < 8; columns++) {
				boardGridRepresentation.getCellFormatter().setStyleName(rows,
						columns, "field_faded");
			}
		}
	}
	
	public void setBoardtoStandardLook() {
		// This hole thing only creates the Board and sets the CSS Styles for
		// the Elements
		boolean evenrow = false;
		boolean evencolumn = false;

		for (int rows = 0; rows < 8; rows++) {
			for (int columns = 0; columns < 8; columns++) {

				if (evenrow && evencolumn) {
					boardGridRepresentation.getCellFormatter().setStyleName(
							rows, columns, "field_black");
				} else if (evenrow && !evencolumn) {
					boardGridRepresentation.getCellFormatter().setStyleName(
							rows, columns, "field_white");
				} else if (!evenrow && evencolumn) {
					boardGridRepresentation.getCellFormatter().setStyleName(
							rows, columns, "field_white");
				} else { // !evenrow && !evencolumn
					boardGridRepresentation.getCellFormatter().setStyleName(
							rows, columns, "field_black");
				}
				if (evencolumn) {
					evencolumn = false;
				} else {
					evencolumn = true;
				}
			}
			if (evenrow) {
				evenrow = false;
			} else {
				evenrow = true;
			}
			;
		}
		// it's a trap!
		boardGridRepresentation.getCellFormatter().setStyleName(2, 2,
				"field_trap");
		boardGridRepresentation.getCellFormatter().setStyleName(2, 5,
				"field_trap");
		boardGridRepresentation.getCellFormatter().setStyleName(5, 2,
				"field_trap");
		boardGridRepresentation.getCellFormatter().setStyleName(5, 5,
				"field_trap");
	}
	
	public void addBoardToRootPanel() {
		RootPanel.get("board").add(boardGridRepresentation);
	}

	public GuiPiece[] getStartConfiguration(){
		return startConfiguration;
	}
	
	public void setOpponentsStartConfigurationToTheBoard(GuiPiece[] startConfigToBeAdded) {
		for (int i = 0; i < startConfigToBeAdded.length; i++) {
			if (i < 8) {
				boardGridRepresentation.setWidget(0, i,
						startConfigToBeAdded[i].getImage());
			} else {
				boardGridRepresentation.setWidget(1, i - 8,
						startConfigToBeAdded[i].getImage());
			}
		}
	}
	
	public void setClickHandlerToPlayGameHandler(){
		
		handlerRegistration.removeHandler();
		
		clickHandler =  new PlayGameClickHandler();	
		((PlayGameClickHandler) clickHandler).setBoardGridRepresentation(boardGridRepresentation);
		((PlayGameClickHandler) clickHandler).setBoard(this);
		
		handlerRegistration = boardGridRepresentation.
				addClickHandler(clickHandler);
	}

	public int calculatePostionInStartConfig(int xPosition, int yPostion) {
		if (yPostion == 7) {
			return xPosition;
		} else if (yPostion == 6) {
			return (xPosition + 8);
		} else {
			Window.alert("How on earth did you put a piece there?");
			return -1;
		}
	}

	public void setPieceOnTheBoardDuringStartConfig(GuiPiece piece, int xcord,
			int ycord) {
		if (ycord < 6) {
			Window.alert("you cant put a piece here!");
		} else {
			boardGridRepresentation.setWidget(ycord, xcord, piece.getImage());
		}
	}

	public void setLastBoxClicked(SinglePieceBoxPanel singlePieceBoxPanel) {
		lastClickedBox = singlePieceBoxPanel;
	}

	public SinglePieceBoxPanel getLastClickedBox(){
		return lastClickedBox;
	}
	
	public void setChoosenPiece(GuiPiece piece) {
		this.choosenPiece = piece;
	}
	
	public GuiPiece getChoosenPiece(){
		return choosenPiece;
	}
}
