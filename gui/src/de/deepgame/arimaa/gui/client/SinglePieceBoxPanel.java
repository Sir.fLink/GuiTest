package de.deepgame.arimaa.gui.client;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

import de.deepgame.arimaa.gui.shared.GuiPiece;

/**
 * 
 * @author haukeedeler Constructs the single Panels for the Pieces-Box (you need
 *         one for every piece)
 */

public class SinglePieceBoxPanel extends Composite implements ClickHandler {
	
	private GuiPiece piece;
	private VerticalPanel singlePiecePanel = new VerticalPanel();
	private FlowPanel imagePanel = new FlowPanel();
	private Image pieceImage;
	private Board board = Board.getInstance();
	private Label piecesLeftLabel;

	// 0 = no pieces of this type Left
	private int counter;

	SinglePieceBoxPanel(GuiPiece piece) {
		this.piece = piece;
		pieceImage = piece.getImage();
		pieceImage.addClickHandler(this);
		imagePanel.add(pieceImage);
		singlePiecePanel.add(imagePanel);

		switch (piece) {
		case G_RABBIT:
			this.counter = 8;
			break;

		case G_CAT:
			this.counter = 2;
			break;

		case G_DOG:
			this.counter = 2;
			break;

		case G_HORSE:
			this.counter = 2;
			break;

		case G_CAMEL:
			this.counter = 1;
			break;

		case G_ELEPHANT:
			this.counter = 1;
			break;

		default:
			break;
		}

		piecesLeftLabel = new Label("x" + counter);
		singlePiecePanel.add(piecesLeftLabel);
		initWidget(singlePiecePanel);
	}

	@Override
	public void onClick(ClickEvent event) {
			board.setChoosenPiece(this.piece);
			board.setLastBoxClicked(this);			
	}

	public int getCounter() {
		return counter;
	}

	public void decrementCounter() {
		this.counter = this.counter - 1;
		this.piecesLeftLabel.setText("x" + this.counter);
		if (counter == 0) {
			this.pieceImage.removeFromParent();
			this.imagePanel.add(this.piece.getEmptyImage());
			
		}
	}
}
