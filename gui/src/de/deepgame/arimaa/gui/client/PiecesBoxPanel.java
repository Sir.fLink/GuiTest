package de.deepgame.arimaa.gui.client;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.sun.corba.se.impl.interceptors.PINoOpHandlerImpl;

import de.deepgame.arimaa.gui.shared.GuiPiece;

public class PiecesBoxPanel extends Composite {

	static PiecesBoxPanel instance;

	SinglePieceBoxPanel rabbit = new SinglePieceBoxPanel(GuiPiece.G_RABBIT);
	SinglePieceBoxPanel cat = new SinglePieceBoxPanel(GuiPiece.G_CAT);
	SinglePieceBoxPanel dog = new SinglePieceBoxPanel(GuiPiece.G_DOG);
	SinglePieceBoxPanel horse = new SinglePieceBoxPanel(GuiPiece.G_HORSE);
	SinglePieceBoxPanel camel = new SinglePieceBoxPanel(GuiPiece.G_CAMEL);
	SinglePieceBoxPanel elephant = new SinglePieceBoxPanel(GuiPiece.G_ELEPHANT);

	private PiecesBoxPanel() {
		// set up PieceBox
		HorizontalPanel piecePanel = new HorizontalPanel();
		piecePanel.setStyleName("piece_table");

		// add PieceBox Panels for all Pieces
		piecePanel.add(new SinglePieceBoxPanel(GuiPiece.G_RABBIT));
		piecePanel.add(new SinglePieceBoxPanel(GuiPiece.G_CAT));
		piecePanel.add(new SinglePieceBoxPanel(GuiPiece.G_DOG));
		piecePanel.add(new SinglePieceBoxPanel(GuiPiece.G_HORSE));
		piecePanel.add(new SinglePieceBoxPanel(GuiPiece.G_CAMEL));
		piecePanel.add(new SinglePieceBoxPanel(GuiPiece.G_ELEPHANT));
		
		initWidget(piecePanel);

	}
	
	/**
	 * Removes the Pieces Box from the Screen 
	 */
	public void removePiecesBox(){
		RootPanel.get("piece_box").removeFromParent();
	};
	
	public static PiecesBoxPanel getInstance(){
		if (instance == null){
			instance = new PiecesBoxPanel();
		}
		return instance;
	}

}
