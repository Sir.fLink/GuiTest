package de.deepgame.arimaa.gui.client;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Grid;

public class StartConfigClickHandler implements ClickHandler {

	private Grid boardGridRepresentation;
	private Board board;

	public void setBoard(Board board) {
		this.board = board;
	}

	public void setBoardGridRepresentation(Grid boardGridRepresentation) {
		this.boardGridRepresentation = boardGridRepresentation;
	}

	@Override
	public void onClick(ClickEvent event) {
		int xOfClick = boardGridRepresentation.getCellForEvent(event)
				.getCellIndex();
		int yOfClick = boardGridRepresentation.getCellForEvent(event)
				.getRowIndex();
				
		if (!(yOfClick < 6)
				&& board.getStartConfiguration()[board
						.calculatePostionInStartConfig(xOfClick, yOfClick)] == null && board.getChoosenPiece() != null && board.getLastClickedBox() != null) {
			
			// Set the Piece on the field
			board.setPieceOnTheBoardDuringStartConfig(board.getChoosenPiece(), xOfClick,
					yOfClick);
			// decrement the Counter for the Number of pieces Left
			board.getLastClickedBox().decrementCounter();

			// Store the Piece in the StartConfig Array for the GC
			board.getStartConfiguration()[board.calculatePostionInStartConfig(xOfClick, yOfClick)] = board.getChoosenPiece();

			board.setChoosenPiece(null);
			board.setLastBoxClicked(null);
		}
	}
}
