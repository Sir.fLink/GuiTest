package de.deepgame.arimaa.gui.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.RootPanel;

import de.deepgame.arimaa.gui.shared.GuiPiece;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Gui implements EntryPoint {
	
	private StartGameServiceAsync startGameServiceAsync;

	/**
	 * get the refrence to the Gui-Board (Singelton)
	 */
	//private GuiBoard guiBoard = GuiBoard.getInstance();
	private Board board = Board.getInstance();
		
	/**
	 * This is the entry point method, called when the page is loaded
	 */
	public void onModuleLoad() {
				
		// fade the upper rows of the board
		//guiBoard.setFadeForStartConfig();
		
		// get the piece_box <div> and add the piecePanel (Singleton)
		RootPanel.get("piece_box").add(PiecesBoxPanel.getInstance());

		// tell the board to add itself to the RootPanel
		board.addBoardToRootPanel();

		// This Button sends the StartConfig to the server. If that is
		// successful it
		// also gets the opponents startconfig and sets it to the Board
		final Button startGame = new Button("StartGame");
		startGame.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				if (sendStartConfigToTheServer()) {
					// since the game has been started, remove the Button
					// TODO: This is no good solution. If the Call to the server
					// dies on the way the button will still be removed...
					startGame.removeFromParent();
					board.setBoardtoStandardLook();
				}
			}
		});

		// adds the Button to the RootPanel...
		startGame.setStyleName("yes_button");
		RootPanel.get().add(startGame);
	}
	
	public boolean sendStartConfigToTheServer() {
		/**
		 * Create a remote service proxy to talk to the server-side service.
		 */
		if (startGameServiceAsync == null) {
			startGameServiceAsync = GWT.create(StartGameService.class);
		}

		if (board.getStartConfiguration().length != 16) {
			// TODO: Allow the player to play with a Handicap (aka fewer pieces)
			Window.alert("You have to put all Pieces on the board!");
			return false;
		} else {
			startGameServiceAsync.sendStartConfig(board.getStartConfiguration(),
					new AsyncCallback<String>() {

						@Override
						public void onSuccess(String result) {
							// TODO The Result could be logged...

							// if setting the StartConfig on the Server is
							// successful, retrieve the opponents startconfig
							// from the Server

							getOpponentStartConfigFromTheServer();
							board.setClickHandlerToPlayGameHandler();
						}

						@Override
						public void onFailure(Throwable caught) {
							Window.alert("Communication went wrong while sendig the StartConfiguration to the Server");

						}
					});
		}
		return true;
	}

	private void getOpponentStartConfigFromTheServer() {
		startGameServiceAsync
				.getOpponentStartConfig(new AsyncCallback<GuiPiece[]>() {

					@Override
					public void onSuccess(GuiPiece[] result) {
						// Set the board to standard-look
						GuiBoard.getInstance().setBoardtoStandardLook();
						// Remove the Pieces Box from Screen
						PiecesBoxPanel.getInstance().removeFromParent();

						// Set the opponents
						// StartConfiguration on the Board
						board.setOpponentsStartConfigurationToTheBoard(result);
					}

					@Override
					public void onFailure(Throwable caught) {
						Window.alert("Was not able to get the opponents StartConfig from the Server.");
					}
				});
	}
}