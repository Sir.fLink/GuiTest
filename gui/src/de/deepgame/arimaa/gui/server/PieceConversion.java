package de.deepgame.arimaa.gui.server;

import de.deepgame.arimaa.gui.shared.GuiPiece;
import deep_game.arimaa.game.Piece;
import deep_game.arimaa.game.Piece.Type;

public class PieceConversion {
	
	/**
	 * Converts a GuiPiece to Piece compatible with the Game Controller
	 * 
	 * @param pieceToBeChecked
	 * @return
	 */
	public static Piece guiPieceToGCPiece(GuiPiece pieceToBeChecked) {
		// True in the piece Constructor means the piece is gold...
		Piece pieceToBeAdded = null;

		switch (pieceToBeChecked) {

		case G_RABBIT:
			pieceToBeAdded = new Piece(Type.RABBIT, true);
			break;

		case G_CAT:
			pieceToBeAdded = new Piece(Type.CAT, true);
			break;

		case G_DOG:
			pieceToBeAdded = new Piece(Type.DOG, true);
			break;

		case G_HORSE:
			pieceToBeAdded = new Piece(Type.HORSE, true);
			break;

		case G_CAMEL:
			pieceToBeAdded = new Piece(Type.CAMEL, true);
			break;

		case G_ELEPHANT:
			pieceToBeAdded = new Piece(Type.ELEPHANT, true);
			break;

		case S_RABBIT:
			pieceToBeAdded = new Piece(Type.RABBIT, false);
			break;

		case S_CAT:
			pieceToBeAdded = new Piece(Type.CAT, false);

		case S_DOG:
			pieceToBeAdded = new Piece(Type.DOG, false);
			break;

		case S_HORSE:
			pieceToBeAdded = new Piece(Type.HORSE, false);

		case S_CAMEL:
			pieceToBeAdded = new Piece(Type.CAMEL, false);
			break;

		case S_ELEPHANT:
			pieceToBeAdded = new Piece(Type.ELEPHANT, false);
			break;

		default:

		}
		return pieceToBeAdded;
	}
	

	/**
	 * Converts a Piece (GC-compatible) to a GuiPiece
	 */
	public static GuiPiece gcPieceToGuiPiece(Piece pieceToBeChecked) {
		GuiPiece pieceToBeAdded = null;

		if (pieceToBeChecked.isGold()) {
			// give back the golden version

			switch (pieceToBeChecked.getType()) {
			case RABBIT:
				pieceToBeAdded = GuiPiece.G_RABBIT;
				break;

			case CAT:
				pieceToBeAdded = GuiPiece.G_CAT;
				break;

			case DOG:
				pieceToBeAdded = GuiPiece.G_DOG;
				break;

			case HORSE:
				pieceToBeAdded = GuiPiece.G_HORSE;
				break;

			case CAMEL:
				pieceToBeAdded = GuiPiece.G_CAMEL;
				break;

			case ELEPHANT:
				pieceToBeAdded = GuiPiece.G_ELEPHANT;
				break;

			default:
				break;
			}

		} else {
			// give back the silver version
			switch (pieceToBeChecked.getType()) {
			case RABBIT:
				pieceToBeAdded = GuiPiece.S_RABBIT;
				break;

			case CAT:
				pieceToBeAdded = GuiPiece.S_CAT;
				break;

			case DOG:
				pieceToBeAdded = GuiPiece.S_DOG;
				break;

			case HORSE:
				pieceToBeAdded = GuiPiece.S_HORSE;
				break;

			case CAMEL:
				pieceToBeAdded = GuiPiece.S_CAMEL;
				break;

			case ELEPHANT:
				pieceToBeAdded = GuiPiece.S_ELEPHANT;
				break;

			default:
				break;
			}
		}

		return pieceToBeAdded;
	}
	
	
}
