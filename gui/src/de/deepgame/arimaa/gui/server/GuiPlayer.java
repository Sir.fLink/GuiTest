package de.deepgame.arimaa.gui.server;

import java.util.Arrays;

import deep_game.arimaa.game.Field;
import deep_game.arimaa.game.GameController;
import deep_game.arimaa.game.Piece;
import deep_game.arimaa.game.Turn;
import deep_game.arimaa.player.Player;

public class GuiPlayer extends Player {

	private Piece[] startConfig = null;
	private static GuiPlayer player1;
	private static GuiPlayer player2;
	private StartGameServiceImpl startGameService;
	private GameController gameController;
	
	private GuiPlayer() {
	}

	/**
	 * This method is called by the GC to set the StartConfig of the Opponent.
	 */
	@Override
	public void setStartConfig(Piece[] startConfig, boolean gold) {
		if (!gold) {
			startGameService.setOpponentStartConfiguration(startConfig);

		} else {
			// TODO Unimplemented: The Opponent is Gold
			System.out.println("The oterher Player is Gold too...");
		}

	};

	@Override
	public Piece[] getStartConfig() {
		System.out.println(">>> getStartConfig() in GuiPlayer is called");

		System.out.println(Arrays.toString(startConfig));
		return startConfig;
	}

	
	@Override
	public void youLost() {
		System.out.println(">>> youLost() in GuiPlayer is called");
		// TODO show the user a "you lost"-message
	}

	@Override
	public void youWon() {
		System.out.println(">>> youWon() in GuiPlayer is called");
		// TODO show the user a "you won"-message s

	}

	public void storeStartConfiguration(Piece[] startConfig) {
		this.startConfig = startConfig;
	}

	public static GuiPlayer getInstancePlayerOne() {
		if (player1 == null) {
			player1 = new GuiPlayer();
		}
		return player1;
	}

	// TODO make sure this can't be called in a singleplayer (aka. human-vs-bot
	// game)
	public static GuiPlayer getInstancePlayerTwo() {
		if (player2 == null) {
			player2 = new GuiPlayer();
		}
		return player2;
	}

	/**
	 * Only used to get the refrence. This is horrible.
	 * 
	 * @param startGameService
	 */
	public void setStartGameService(StartGameServiceImpl startGameService) {
		this.startGameService = startGameService;
	}
	
	public void setGameController(GameController gameController){
		this.gameController = gameController;
	}
	
	public GameController getGameController(){
		return gameController;
	}

	@Override
	public Turn getTurn() {
		// TODO Auto-generated method stub
		return null;
	}
}
