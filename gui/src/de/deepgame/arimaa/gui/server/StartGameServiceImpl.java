package de.deepgame.arimaa.gui.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import de.deepgame.arimaa.gui.client.StartGameService;
import de.deepgame.arimaa.gui.shared.GuiPiece;
import deep_game.arimaa.game.GameController;
import deep_game.arimaa.game.Piece;
import deep_game.arimaa.player.bot.SmartBotPlayer;

/**
 * The server-side implementation of the RPC service.
 */

@SuppressWarnings("serial")
public class StartGameServiceImpl extends RemoteServiceServlet implements
		StartGameService {

	GuiPlayer guiPlayer;
	SmartBotPlayer smartBotPlayer;
	GameController gameController;
	Piece[] gcStartConfiguration = new Piece[16];
	Piece[] opponentStartConfiguration;

	/**
	 * This Method is called by the Gui Player to pass the Opponents StartConfig
	 * 
	 * @param opponentStartConfiguration
	 */
	public void setOpponentStartConfiguration(Piece[] opponentStartConfiguration) {
		this.opponentStartConfiguration = opponentStartConfiguration;
	}

	/**
	 * This functions starts the GameController in its own Thread on the Server
	 */
	public String sendStartConfig(GuiPiece[] guiStartConfiguration) {

		// Pass the Refrence to the GuiPlayer for getting the StartConfig of the
		// Opponent later... -.-
		GuiPlayer.getInstancePlayerOne().setStartGameService(this);

		// transcode the GuiPiece-Array into regular Piece-Array
		for (int i = 0; i < guiStartConfiguration.length; i++) {
			gcStartConfiguration[i] = PieceConversion
					.guiPieceToGCPiece(guiStartConfiguration[i]);
		}

		// store the StartConfiguration in the GuiPlayer so the GameController
		// can take it from there
		GuiPlayer.getInstancePlayerOne().storeStartConfiguration(
				gcStartConfiguration);

		// Start the Game...
		guiPlayer = GuiPlayer.getInstancePlayerOne();

		smartBotPlayer = new SmartBotPlayer();
		gameController = new GameController(guiPlayer, smartBotPlayer);

		// start Game without asking for pressing enter to start
		// gameController.startGame(false);

		// Start the GC in its own Thread, so the Method can return
		Thread t = new Thread() {
			public void run() {
				gameController.startGame(false);
			}
		};
		t.start();

		return "The Gamecontroller was started on the Server";
	}

	/**
	 * Some Demo service. more or less copied from the GWT-Tutorial... ;)
	 */
	public String greetServer(String input) throws IllegalArgumentException {
		// guiPlayer = GuiPlayer.getInstancePlayerOne();
		// smartBotPlayer = new SmartBotPlayer();
		// gameController = new GameController(guiPlayer, smartBotPlayer);
		// // start Game without asking for pressing enter to start
		// gameController.startGame(false);
		return "nothing done in greetServer()";
	}

	/**
	 * Used by the Client to get a GuiPiece-Array of the Opponents
	 * StartConfiguration
	 */
	public GuiPiece[] getOpponentStartConfig() {

		// TODO THIS DOES NOT WORK

		if (opponentStartConfiguration == null) {
			System.out.println("there is no Startconfig");
		}

		GuiPiece[] guiPieces = new GuiPiece[16];

		for (int i = 0; i < opponentStartConfiguration.length; i++) {
			guiPieces[i] = PieceConversion
					.gcPieceToGuiPiece(opponentStartConfiguration[i]);
		}

		return guiPieces;
	}
}
