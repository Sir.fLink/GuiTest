package de.deepgame.arimaa.gui.shared;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;

public enum GuiPiece {
	G_RABBIT, G_CAT, G_DOG, G_HORSE, G_CAMEL, G_ELEPHANT, S_RABBIT, S_CAT, S_DOG, S_HORSE, S_CAMEL, S_ELEPHANT, E_RABBIT, E_CAT, E_DOG, E_HORSE, E_CAMEL, E_ELEPHANT;

	/**
	 * Gets an gwt-image for the piece NOTE: Images are hard to refresh. A
	 * change in the URL does not trigger a force-refresh. So Always put images
	 * on their own panel. For refresh then remove the Image from the Panel and
	 * add a new one. 
	 * 
	 * @return
	 */
	public Image getImage() {
		Image image = null;
		
		switch (this) {

		case G_RABBIT:
			image = new Image(GWT.getModuleBaseURL() + "images/g_rabbit.png");
			break;

		case G_CAT:
			image = new Image(GWT.getModuleBaseURL() + "images/g_cat.png");
			break;

		case G_DOG:
			image = new Image(GWT.getModuleBaseURL() + "images/g_doge.png");
			break;

		case G_HORSE:
			image = new Image(GWT.getModuleBaseURL() + "images/g_horse.png");
			break;

		case G_CAMEL:
			image = new Image(GWT.getModuleBaseURL() + "images/g_camel.png");
			break;

		case G_ELEPHANT:
			image = new Image(GWT.getModuleBaseURL() + "images/g_elephant.png");
			break;

		case S_RABBIT:
			image = new Image(GWT.getModuleBaseURL() + "images/s_rabbit.png");
			break;

		case S_CAT:
			image = new Image(GWT.getModuleBaseURL() + "images/s_cat.png");
			break;

		case S_DOG:
			image = new Image(GWT.getModuleBaseURL() + "images/s_doge.png");
			break;

		case S_HORSE:
			image = new Image(GWT.getModuleBaseURL() + "images/s_horse.png");
			break; 
			
		case S_CAMEL:
			image = new Image(GWT.getModuleBaseURL() + "images/s_camel.png");
			break;

		case S_ELEPHANT:
			image = new Image(GWT.getModuleBaseURL() + "images/s_elephant.png");
			break;

		case E_RABBIT:
			image = new Image(GWT.getModuleBaseURL() + "images/e_rabbit.png");
			break;

		case E_CAT:
			image = new Image(GWT.getModuleBaseURL() + "images/e_cat.png");
			break;

		case E_DOG:
			image = new Image(GWT.getModuleBaseURL() + "images/e_doge.png");
			break;

		case E_HORSE:
			image = new Image(GWT.getModuleBaseURL() + "images/e_horse.png");
			break;

		case E_CAMEL:
			image = new Image(GWT.getModuleBaseURL() + "images/e_camel.png");
			break;

		case E_ELEPHANT:
			image = new Image(GWT.getModuleBaseURL() + "images/e_elephant.png");
			break;

		default:
			break;
		}

		return image;
	}

	/**
	 * gives Back the empty (non-colored) Version of a piece
	 * 
	 * @return
	 */

	public Image getEmptyImage() {
		Image emptyImage;

		switch (this) {
		case G_RABBIT:
		case S_RABBIT:
			emptyImage = GuiPiece.E_RABBIT.getImage();
			break;

		case G_CAT:
		case S_CAT:
			emptyImage = GuiPiece.E_CAT.getImage();
			break;

		case G_DOG:
		case S_DOG:
			emptyImage = GuiPiece.E_DOG.getImage();
			break;

		case G_HORSE:
		case S_HORSE:
			emptyImage = GuiPiece.E_HORSE.getImage();
			break;

		case G_CAMEL:
		case S_CAMEL:
			emptyImage = GuiPiece.E_CAMEL.getImage();
			break;

		case G_ELEPHANT:
		case S_ELEPHANT:
			emptyImage = GuiPiece.E_ELEPHANT.getImage();
			break;

		default:
			emptyImage = null;

		}
		return emptyImage;
	}
}
